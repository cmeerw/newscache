#ifndef _SETUGID_H_
#define _SETUGID_H_

extern void setugid(const char *uname, const char *gname);

#endif

/*
 * Local Variables:
 * mode: c++
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 */
